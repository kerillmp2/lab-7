#include "mem.h"

void* _malloc( size_t size ) {
	if ( size == 0 ) {
		return NULL;
	}
	if (size < _PAGESIZE ) {
		size = _PAGESIZE;
	}

	struct memory* block = find_free_block( size );

	if ( !block ) {
		block = generate_node( size );
	}

	return choose_block( block, size );
}

void _free( void* memory ) {
	struct memory* current_block = heap_start;
	struct memory* last_block = NULL;

	while( current_block != NULL ) {
		if( ( current_block + 1 ) == memory ) {
			current_block->is_free = true;
			merge( last_block, current_block );
			merge( current_block, current_block->next );
			return;
		}
		last_block = current_block;
		current_block = current_block->next;
	}
}

void memalloc_reset( void ) {
	struct memory* current_block = heap_start;
	struct memory* last_block;

	while( current_block != NULL ) {
		last_block = current_block;
		current_block = current_block->next;
		munmap( last_block, sizeof( struct memory ) + last_block->capacity );
	}

	heap_start = NULL;
}

void* heap_init( size_t inital_size ) {
	return _malloc( inital_size );
}

struct memory* generate_node( size_t size ) {

	struct memory* last_block = get_last_block();
	struct memory* new_block;
	void* last_address = get_last_node( last_block );
	void* dedicated_address;
	size_t length = sizeof( struct memory ) + size;

	if( last_address == NULL ) {
		last_address = HEAP_BEGIN;
	}

	dedicated_address = mmap( last_address, length, MMAP_PROT, MMAP_FLAGS, -1, 0 );
	if( dedicated_address == ( void* ) -1 ) {
		return NULL;
	}

	new_block = dedicated_address;
	new_block->capacity = size;
	new_block->is_free = true;
	new_block->next = NULL;

	if ( last_block != NULL ) {
		last_block->next = new_block;
		if ( merge( last_block, new_block ) ) {
			return last_block;
		}
		return new_block;
	}

	heap_start = new_block;
	return new_block;
}

void* choose_block( struct memory* block, size_t capacity ) {
	struct memory* new_block;
	size_t prev_block_capacity;
	size_t next_block_capacity;

	if( block == NULL ) {
		return NULL;
	}

	prev_block_capacity = capacity;
	next_block_capacity = block->capacity - capacity - sizeof( struct memory );
	block->is_free = false;

	if( block->capacity != capacity && next_block_capacity >= _PAGESIZE ) {
		block->capacity = prev_block_capacity;
		new_block = get_last_node( block );
		new_block->capacity = next_block_capacity;
		new_block->next = block->next;
		new_block->is_free = true;
		block->next = new_block;
	}

	return ( block + 1 );
}

struct memory* find_free_block( size_t size ) {
	struct memory* current_block = heap_start;
	while ( current_block != NULL ) {
		if( current_block->is_free && current_block->capacity >= size ) {
			break;
		}
		current_block = current_block->next;
	}
	return current_block;
}

void* get_last_node( struct memory* block ) {
	if( block == NULL ) {
		return NULL;
	}

	return ( ( (int8_t*) (block + 1) ) + ( block->capacity ) );
}

struct memory* get_last_block( void ) {
	struct memory* current_memory = heap_start;

	if ( current_memory == NULL ) {
		return NULL;
	}

	while ( current_memory->next ) {
		current_memory = current_memory->next;
	}

	return current_memory;
}

bool merge( struct memory* begin, struct memory* end ) {
	struct memory* swapper;
	if ( begin == NULL || end == NULL ) {
		return false;
	}

	if ( !( begin->is_free ) || !( end->is_free ) ) {
		return false;
	}

	if ( end->next == begin ) {
		swapper = begin;
		begin = end;
		end = swapper;
	}

	if ( begin->next != end ) {
		return false;
	}

	if ( get_last_node( begin ) == end ) {
		begin->next = end->next;
		begin->capacity += ( end->capacity + sizeof( struct memory ) );
		return true;
	}
	return false;
}

void memalloc_debug_trace( void ) {
	struct memory* current_block = heap_start;
	int i = 1;

	if( current_block == NULL ) {
		puts( "There are no blocks in heap" );
	}
	while ( current_block ) {
		printf( "%d: %p, size: %zu bytes, block is %s\n", i, ( void* ) ( current_block + 1 ), ( current_block->capacity ), ( current_block->is_free ? "free" : "busy" ) );
		current_block = current_block->next;
		i += 1;
	}
}
