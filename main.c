#include <stdio.h>
#include "mem.h"

void* allocate_mem( size_t size ) {
	void* address;

	printf( "Allocating %zu bytes...\n", size );
	address = _malloc( size );
	printf( "%zu bytes allocated successfully\n", size );

	return address;
}

void free_mem( void* address ) {
	printf( "Free address %p...\n", ( void* ) address );
	_free( address );
	printf( "Address %p released\n", ( void* ) address );

	return;
}

void info_mem( void ) {
	puts("\nMemory debug trace:");
	memalloc_debug_trace();
	puts("");
}

void reset_mem( void ) {
	puts("Reseting memory...");
	memalloc_reset();
	puts("Memory reseted");
}

int main( void ) {
	size_t test_1 = 2837203354;
	size_t test_2 = 3619018495;
	size_t test_3 = 1111992812;
	void* address_1;
	void* address_2;
	void* address_3;

	address_1 = allocate_mem( test_1 );
	address_2 = allocate_mem( test_2 );
	address_3 = allocate_mem( test_3 );

	info_mem();

	free_mem( address_2 );
	info_mem();
	free_mem( address_1 );
	free_mem( address_3 );
	info_mem();

	reset_mem();
	info_mem();

	return 0;
}
