#ifndef _MEM_H_
#define _MEM_H_
#define _USE_MICS_

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>

#define HEAP_BEGIN ( ( void* ) 0x04040000 )
#define _PAGESIZE 4096

#define MMAP_PROT PROT_READ | PROT_WRITE
#define MMAP_FLAGS MAP_PRIVATE | MAP_ANONYMOUS

struct memory;

#pragma pack( push, 1 )
struct memory {
	struct memory* next;
	size_t capacity;
	bool is_free;
};
#pragma pack( pop )

struct memory* heap_start;

void* _malloc( size_t );
void _free( void* );
void* heap_init( size_t );
struct memory* find_free_block( size_t );
void* get_last_node( struct memory* );
struct memory* generate_node( size_t );
struct memory* get_last_block();
bool merge( struct memory*, struct memory* );
void* choose_block( struct memory*, size_t );
void memalloc_reset( void );
void memalloc_debug_trace( void );

#endif
